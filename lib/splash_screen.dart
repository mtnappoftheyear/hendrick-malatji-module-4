import 'dart:async';
import 'package:flutter/material.dart';
// ignore: depend_on_referenced_packages
import 'package:module_04/login.dart';
// import 'package:login.dart';
// import 'package:registration.dart';
// import 'package:go_plus/size_config.dart';

class SplashScreen extends StatefulWidget {
  static String routeName = "/splash";

  SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin{
  var _visible = true;

  late AnimationController animationController;
  late Animation<double> animation;
  

  startTime() async {
    var _duration = new Duration(milliseconds: 3500);
    return new Timer(_duration, navigationPage);
  }

  void navigationPage(){
    Navigator.pushNamed(context, Login.routeName);
  }

  @override
  void initState(){
    super.initState();
    animationController = new AnimationController(
      vsync: this,
      duration: new Duration(milliseconds: 2500),
    );
    animation =
        new CurvedAnimation(parent: animationController, curve: Curves.ease);

    animation.addListener(() => this.setState(() {}));
    animationController.forward();

    setState(() { Login;{}
      _visible = !_visible;
    });
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 66, 201, 255),
      body: _body(),
    );
  }

  Stack _body() {
    return Stack(
      fit: StackFit.expand,
      children: <Widget> [
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget> [
            Image.asset(
              "assets/images/fish1.jpg",
              width: animation.value * 200,
              height: animation.value * 200,
            )
          ],
        )
      ],
    );
  }
  
  SizeConfig() {}

}